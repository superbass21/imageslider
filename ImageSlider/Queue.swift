//
//  Queue.swift
//  ImageSlider
//
//  Created by Taewook on 2016. 12. 22..
//  Copyright © 2016년 SaladBox. All rights reserved.
//

import Foundation

class Queue<T> {
    var front: Int = 0
    var rear: Int = 0
    
    var capacity: Int
    var items: [T?]
    
    init(capacity: Int) {
        self.capacity = capacity + 1
        self.items = []
        
        if self.capacity < 10 {
            self.capacity = 10 + 1
        }
    }
    
    func reset() {
        front = 0
        rear = 0
        items = []
    }
    
    func count() -> Int {
        if front <= rear {
            return (rear - front)
        }
        else {
            return capacity - (front - rear)
        }
    }
    
    func isEmpty() -> Bool {
        return front == rear
    }
    
    func isFull() -> Bool {
        return (front == ((rear + 1) % capacity))
    }
    
    func enqueue(element: T) -> Bool {
        if isFull() {
            return false
        }
        
        if items.count < capacity {
            items.append(element)
        }
        else {
            items[rear] = element
        }

        rear = (rear + 1) % capacity
        
        return true
    }
    
    func dequeue() -> T? {
        if isEmpty() {
            return nil
        }

        let value = items[front]
        front = (front + 1) % capacity
        
        return value
    }
}