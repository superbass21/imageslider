//
//  ViewController.swift
//  ImageSlider
//
//  Created by Taewook on 2016. 12. 22..
//  Copyright © 2016년 SaladBox. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ImageLoaderDelegate {
    @IBOutlet weak var controlView: UIView!
    @IBOutlet weak var delaySlider: UISlider!
    @IBOutlet weak var delayLabel: UILabel!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    let animationDuration: NSTimeInterval = 0.3
    var currentImageView: UIImageView? = nil
    var imageContainerView: UIView!
    let imageLoader = ImageLoader()
    var timer: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageLoader.delegate = self
        
        imageContainerView = UIView()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.stopToTapAction(_:)))
        imageContainerView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        print("View will transition to size")
        imageContainerView.frame = CGRectMake(0, 0, size.width, size.height)
    }

    func changeDelayLabel(seconds: Int) {
        var text = "slide image after \(seconds) second"
        if 1 < seconds {
            text += "s"
        }
        
        delayLabel.text = text
    }
    
    func timerAction() {
        let image = imageLoader.getNextImageIfExist()
        guard image != nil else {
            return
        }
        
        let imageView = UIImageView(image: image)
        imageView.frame = imageContainerView.bounds
        imageView.contentMode = .ScaleAspectFit
        imageView.alpha = 0.1
        imageContainerView.addSubview(imageView)
        
        UIView.animateWithDuration(animationDuration, animations: {
            self.currentImageView!.alpha = 0.1
            imageView.alpha = 1.0
            }) { (complete) in
                self.currentImageView!.removeFromSuperview()
                self.currentImageView = imageView
        }
    }
    
    func startSlide() {
        imageContainerView.frame = self.view.bounds
        let image = imageLoader.getNextImageIfExist()
        guard image != nil else {
            return
        }
        
        let imageView = UIImageView(image: image)
        imageView.frame = imageContainerView.bounds
        imageView.contentMode = .ScaleAspectFit
        imageContainerView.addSubview(imageView)
        currentImageView = imageView
        
        imageContainerView.alpha = 0.1
        self.view.addSubview(imageContainerView)
        
        UIView.animateWithDuration(animationDuration, animations: {
            self.imageContainerView.alpha = 1.0
            self.controlView.alpha = 0.1
            }) { (complete) in
                self.controlView.hidden = true
        }
        
        let delay = self.delaySlider.value
        self.timer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(delay), target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        
        print("Start Slide")
    }
    
    func stopSlide() {
        if timer != nil && timer.valid {
            timer.invalidate()
            timer = nil
        }
        
        controlView.hidden = false
        UIView.animateWithDuration(animationDuration, animations: {
            self.controlView.alpha = 1.0
            self.imageContainerView.alpha = 0.1
            }) { (complete) in
                self.imageContainerView.removeFromSuperview()
                if self.currentImageView != nil {
                    self.currentImageView?.removeFromSuperview()
                }
        }

        print("Stop Slide")
    }
    
    func stopToTapAction(recognizer: UITapGestureRecognizer) {
        stopSlide()
    }

    @IBAction func sliderAction(sender: AnyObject) {
        let delay = delaySlider.value
        let intDelay = round(delay)
        
        delaySlider.value = intDelay
        changeDelayLabel(Int(intDelay))
    }
    
    @IBAction func startButtonAction(sender: AnyObject) {
        startSlide()
    }
    
    func imagesDidLoaded() {
        loadingLabel.removeFromSuperview()
        startButton.hidden = false
    }
}

