//
//  ImageLoader.swift
//  ImageSlider
//
//  Created by Taewook on 2016. 12. 22..
//  Copyright © 2016년 SaladBox. All rights reserved.
//

import UIKit

class ImageLoader {
    let apiAddress = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"
    var imageQueue = Queue<UIImage>(capacity: 30)
    
    var lockQueue: dispatch_queue_t!
    
    var loadFirstTime: Bool = true
    
    weak var delegate: ImageLoaderDelegate? = nil
    
    init() {
        lockQueue = dispatch_queue_create("lockQueue", DISPATCH_QUEUE_SERIAL)
        loadIfNeeded()
    }
    
    func loadIfNeeded() {
        if 10 < imageQueue.count() {
            return
        }
        
        let url = NSURL(string: apiAddress)
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) { (data, response, error) in
            if error != nil {
                print("image load failed : \(error)")
                return
            }
            
            let statusCode = (response as! NSHTTPURLResponse).statusCode
            if statusCode != 200 {
                print("http request status code : \(statusCode)")
                return
            }
            
            let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
            let fixedString = dataString.stringByReplacingOccurrencesOfString("\\'", withString: "'")
            let fixedData = fixedString.dataUsingEncoding(NSUTF8StringEncoding)!
            
            let jsonData = try! NSJSONSerialization.JSONObjectWithData(fixedData, options: NSJSONReadingOptions()) as? [String : AnyObject]

            let items = jsonData!["items"] as! Array<[String: AnyObject]>
            dispatch_async(self.lockQueue, { 
                for item in items {
                    let media = item["media"] as! [String: AnyObject]
                    let imageUrl = media["m"] as! String
                    let image = UIImage(data: NSData(contentsOfURL: NSURL(string: imageUrl)!)!)!
                    if !self.imageQueue.enqueue(image) {
                        print("image queue is full")
                        break
                    }
                    
                    if self.loadFirstTime && 5 < self.imageQueue.count() {
                        dispatch_async(dispatch_get_main_queue(), { 
                            self.delegate?.imagesDidLoaded()
                        })
                        self.loadFirstTime = false
                    }
                    
                    print("imageUrl : \(imageUrl), count : \(self.imageQueue.count()), front : \(self.imageQueue.front), rear : \(self.imageQueue.rear)")
                }
            })
        }
        task.resume()
    }
    
    func getNextImageIfExist() -> UIImage? {
        let image = imageQueue.dequeue()
        loadIfNeeded()
        return image
    }
}

protocol ImageLoaderDelegate: NSObjectProtocol {
    func imagesDidLoaded()
}
